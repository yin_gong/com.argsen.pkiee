/**
 * UserController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  create: (req, res) => {
    let params = req.params.all()
    User.create({name: params.name, email: params.email, password: params.password}).exec((err, created) => {

      if (err === null) {
        return res.json({notice: created})
      } else {
        return res.json(err)
      }

    })
  },

  list_all: (req, res) => {
    User.find().exec((err, user) => {

      if (err === null) {
        return res.json(user)
      } else {
        return res.json(err)
      }

    })

  },

  call_method: (req, res) => {
    User.native((err, collection) => {
      if (err)
        return res.serverError(err);

			collection.insert({
        "name": "\"Sarah C.\""
      })

    })
  }

};
